int pinCinturon = 5; 
int pinMotor = 2;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinMotor, INPUT);
  pinMode(pinCinturon, INPUT);

}

void loop() {

  if(digitalRead(pinMotor)==0 && digitalRead(pinCinturon)==1)
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
  }
  delay(200);
}
