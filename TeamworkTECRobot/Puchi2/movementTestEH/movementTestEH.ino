/*  Robot TEC: declaración de constantes  */ 
// los pines digitales 2 y 3 controlan el motor derecho 
const int In1 =  2; // pin  motor derecho 
const int In2 =  3; // pin  motor derecho 
// los pines digitales 4 y 5 controlan el motor izquierdo 
const int In3 =  4; // pin  motor izquierdo 
const int In4 =  5; // pin  motor izquierdo 
 //los pines digitales 9 y 10 habilitan los  motores 
const int ENA = 9;   // habilita el motor derecho 
const int ENB = 10; // habilita el motor izquierdo


const int LEFT_SENSOR = 7;
const int RIGHT_SENSOR = 6; 

bool right;
 //configuración de señales para el movimiento de los motores 
 //giro hacia adelante del motor derecho In1=HIGH  In2=LOW 
 //giro hacia adelante del motor izquierdo  In3=HIGH   In4=LOW 
 //giro hacia atrás del motor derecho    In1=LOW   In2=HIGH 
 //giro hacia atrás del motor izquierdo  In3=LOW   In4= HIGH 
 //Parar el motor derecho     In1= In2 //Parar el motor izquierdo   In3= In4   

void setup() 
{
  pinMode(In1,OUTPUT);
  pinMode(In2,OUTPUT);
  
  pinMode(In3,OUTPUT);
  pinMode(In4,OUTPUT);

  pinMode(ENA,OUTPUT);
  pinMode(ENB,OUTPUT);
  
  pinMode(LEFT_SENSOR,INPUT);
  pinMode(RIGHT_SENSOR,INPUT);

  right = true;

  Serial.begin(9600);      
}

void loop()
{  
  delay(20);
  digitalWrite(ENA, HIGH);  
  delay(20);
  digitalWrite(ENB, HIGH);  
  readSensor();  
  digitalWrite(ENA, LOW); 
  delay(20);
  digitalWrite(ENB, LOW);  
  delay(200);
  
}


void forwardMovement()
{

  digitalWrite(In1,HIGH);
  digitalWrite(In2,LOW);
  digitalWrite(In3,HIGH);  
  digitalWrite(In4,LOW);  

}

void backwardMovement()
{   
  digitalWrite(In1,LOW);
  digitalWrite(In2,HIGH);
  digitalWrite(In3,LOW);  
  digitalWrite(In4,HIGH);
   
}

void turnLeftMovement()
{

  digitalWrite(In1,HIGH);
  digitalWrite(In2,LOW);
  digitalWrite(In3,LOW);  
  digitalWrite(In4,HIGH);   
  
}

void turnRightMovement()
{
 
  digitalWrite(In1,LOW);
  digitalWrite(In2,HIGH);
  digitalWrite(In3,HIGH);  
  digitalWrite(In4,LOW);  
  
}

void readSensor()
{

  delay(30);
  if(digitalRead(LEFT_SENSOR) && digitalRead(RIGHT_SENSOR))
  {
    delay(10);
    forwardMovement();
    delay(10);
  }
  else if ( digitalRead(LEFT_SENSOR) && !digitalRead(RIGHT_SENSOR))
  {
    delay(20);
    backwardMovement();
    delay(10);
    turnLeftMovement();
    delay(20);
    right= false;
  }
  else if ( !digitalRead(LEFT_SENSOR) && digitalRead(RIGHT_SENSOR))
  {
    delay(20);
    backwardMovement();
    delay(10);
    turnRightMovement();
    delay(20);
    right= true;
  }
  else
  {
    if(right)
    {
      delay(20);
      backwardMovement();
      delay(15);
      turnLeftMovement();
      delay(17);
    }
    else
    {
      delay(20);
      backwardMovement();
      delay(15);
      turnRightMovement();
      delay(17);
    }
  }
 
  delay(50);
}








