#include <DHT.h>
#include <DHT_U.h>


const int SENSOR = 1;

dht DHT;
void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
DHT.read11(SENSOR );
    
    Serial.print("Current humidity = ");
    Serial.print(DHT.humidity);
    Serial.print("%  ");
    Serial.print("temperature = ");
    Serial.print(DHT.temperature); 
    Serial.println("C  ");
    delay(500);
    
}
