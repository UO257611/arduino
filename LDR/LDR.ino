
const int LDR = A0;
const int LED = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(LDR, INPUT);
  pinMode(LED , OUTPUT);
  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if( analogRead(LDR) >= 980 )
  {
    digitalWrite(LED, HIGH);
    
  }
  else
  {
    digitalWrite(LED,LOW);
  }
  delay(500);
}
