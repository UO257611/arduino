int pinBoton = 2;
int pinMotor = 3;
bool flag = false;

void setup() {
  pinMode(pinBoton, INPUT);// put your setup code here, to run once:
  pinMode(pinMotor, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(pinBoton)==0 && flag ==  false){
    flag = true;
  }
  if(digitalRead(pinBoton)==0 && flag == true){
    flag = false;
  }

  Serial.print(flag);
  if( flag){
    digitalWrite(pinMotor, HIGH);
  }
  else{
    digitalWrite(pinMotor, LOW);    
  }

  delay(200);
}
